# Home-Work
This is a small project to work with until we meet again
in December.

# Objective
It's a combination of topics from the CSS course
and the first 3 days of the Angular course. The idea
is to design a small ng app that takes the ng kick-start
app a little bit further.

# Preparation
## Generate Fake Data
Browse to https://mockaroo.com/ and generate some
fake product data as a JSON file. USe the settings 
in the screen-shot below.

![Mockaroo](img/mockaroo.png)

Save the JSON file. We will use it together with `json-server`, similar as in the `http-client` exercise.

## Image URLs
In the generated data there are random image URLs.
It migth be that some images to not exists. If that
happens, just patch the url in the json file, with an
id that resolves to an existing image.

## Database JSON
Create a DB JSON file with two resources

    {
      "users": []
      "products": []
    }

### Users
Create one or two fake user objects.

    {
        "username": "anna",
        "password": "psst",
        "displayName": "Anna Conda",
        "age": 42,
        "female": true,
        "birthDay": "2020-02-25",
        "profileImage": "https://randomuser.me/api/portraits/women/65.jpg"
    }

You can find other photos and user data at
https://randomuser.me/

Create a TS interface representing a user.

### Products
Insert the list of items from Mockaroo in the
products resource list.

Create a TS interface representing a product.

# Layout

Use CSS grid-layout to define the overall layout of the
app. Incorporate a sidebar, header and footer as ng
components. Insert a `<router-outlet></router-outlet>`
for the main content.

Use the two screen-shots below as *inspiration* for
your design.

![Anonymous layout](img/anonymous.png)

![Anonymous layout](img/signed-in.png)

# Authentication

Create a service for authentication, with the methods
* `signIn(username:string, password:string): User`
* `get user(): User`
* `get signedIn(): boolean`
* `signOut()`

## signIn()
Fetch the user object from json-server using query parameters, as described here
https://github.com/typicode/json-server#filter

    GET /users?username=anna&password=psst

Save the user object in the service. 

*TIP*: If you want to; you can save the user object
in the browser's sessionStorage.

    sessionStorage.setItem('user', JSON.stringify(currentUser))
    JSON.parse( sessionStorage.getItem('user') || '{}' )
    sessionStorage.removeItem('user')

More info here https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage

## user()
Return the current user object, or `undefined`.

## signedIn()
Return `true`, if the current user is defined.
This getter is then used to alter the UI, such as toggling the 
sign-in form and the sign-out button and similar for the sidebar.

## signOut()
Set the current user to `undefined`.


# Routing

Define a routing list, similar as in the *ng kick-start*
exercises. With the page components
* Home
* Product List
* Profile

# Pages
## Home
Show a simple greeting and some nice image.

## Products
First, define a products service that can return all items from 
the json-server. Then, use it to display the products as a table. 

You might have to add some CSS magic to render the
images nicely. Start first without the images. When the
table looks nice, then incorporate the images. Patch any
missing image URLs.

## Profile
First, define a profile service that can retrieve the current
user profile, that means all but the `username` and `password`
properties.

Render all user profile data in a *key: value* tabluar layout.
After an update operation, replace the current user object in
the authentication service.

### Widgets
Implement additional widget components for
* `editable-number`
* `editable-boolean`
* `editable-date`

Use them, including `editable-text`, to provide updates of
individual profile properties.

# Way of Working
It's recommended that you work in groups of two. 

Always perform implementation tasks in short *dev-cycles*.
If something goes wrong back/remove your recent work until
it works again and then start over in smaller steps.
You might want to use GIT and commit frequently.

Organize your src/app/ folder similar to

    src/app/
      app.component.*
      domain/
        user.ts
        product.ts
      services/
        authentication.service.ts
        profile.service.ts
        products.service.ts
      layout/
        header/
        footer/
        sidebar/
      pages/
        home/
        products/
        profile/
      widgets/
        editable-text/
        editable-number/
        editable-boolean/
        editable-date/

You're not required to implement everything. Just view this
exercise as an inspiration for a fun dev experience and
complete as much as you like. You can also extend it with
more features, if you have the time.

**Have fun!** */jens*
