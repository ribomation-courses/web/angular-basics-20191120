# HttpClient Demo

## Preparation

    npm install

## Start the Server

    npm run start-server

Then browse to http://localhost:3000/

## Start the ng Client

    npm run start-client

Then browse to http://localhost:4200/ 

## Build & Run Production Version

    npm run build
    npm run start-server
    npm run start-client-run

Then browse to http://localhost:8000/ 


