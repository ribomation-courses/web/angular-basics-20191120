import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User}       from '../domain/user';
import {take, tap}  from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private baseUrl: string = 'http://localhost:3000/users';

  constructor(private http: HttpClient) {
  }

  get users$(): Observable<User[]> {
    return this.http
      .get<User[]>(this.baseUrl)
      .pipe(
        tap(objs => console.debug('***', objs)),
        take(1)
      );
  }

  create(user: User): Observable<User> {
    const payload = {
      name: user.name,
      age:  user.age
    };
    return this.http
      .post<User>(this.baseUrl, payload)
      .pipe(
        tap(objs => console.debug('***', objs)),
        take(1)
      );
  }


  update(user: User): Observable<User> {
    const payload = {
      name: user.name,
      age:  user.age
    };
    return this.http
      .put<User>(`${this.baseUrl}/${user.id}`, payload)
      .pipe(
        tap(objs => console.debug('***', objs)),
        take(1)
      );
  }

  delete(user: User): Observable<void> {
    return this.http
      .delete<void>(`${this.baseUrl}/${user.id}`)
      .pipe(
        take(1)
      );
  }

}
