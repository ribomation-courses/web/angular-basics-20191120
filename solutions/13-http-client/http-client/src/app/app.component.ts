import {Component, OnInit} from '@angular/core';
import {UsersService}      from './services/users.service';
import {Observable}        from 'rxjs';
import {User}              from './domain/user';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'HttpClient Demo';
  users$: Observable<User[]>;
  currentUser: User;

  constructor(private userSvc: UsersService) {
  }

  ngOnInit(): void {
    this.users$ = this.userSvc.users$;
  }

  addUser() {
    this.currentUser = {
      name: '', age: 25, id: -1
    };
  }

  editUser(user: User) {
    this.currentUser = user;
  }

  save() {
    if (this.currentUser.id < 0) {
      this.userSvc.create(this.currentUser)
        .subscribe(obj => {
          this.currentUser = undefined;
          this.users$      = this.userSvc.users$;
        });
    } else {
      this.userSvc.update(this.currentUser)
        .subscribe(obj => {
          this.currentUser = undefined;
          this.users$      = this.userSvc.users$;
        });
    }
  }

  deleteUser(user: User) {
    this.userSvc.delete(user)
      .subscribe(() => {
        this.users$ = this.userSvc.users$;
      });
  }

}
