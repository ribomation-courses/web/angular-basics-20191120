export class Product {
    constructor(
        public name: string,
        public price: number,
        public itemsInStock: number,
        public isService: boolean,
        public lastUpdated: Date
    ) { }
}

export class ProductGenerator {
    generate(n: number = 5): Product[] {
        const lst: Product[] = [];
        while (n-- > 0) { lst.push(this.mk()); }
        return lst;
    }

    mk(): Product {
        const name = this.pick(this.names);
        const price = this.uniform(10, 10000);
        const count = this.uniform(0, 10);
        const svc = this.bool();
        const last = this.date(-this.uniform(0, 30));
        return new Product(name, price, count, svc, last);
    }

    private pick(arr: any[]): any {
        const idx = Math.floor(Math.random() * arr.length);
        return arr[idx];
    }

    private uniform(lb: number, ub: number): number {
        const width = ub - lb + 1;
        return Math.random() * width + lb;
    }

    private bool(): boolean {
        return Math.random() < 0.5;
    }

    private date(offsetDays: number = 0): Date {
        const days = offsetDays * 24 * 3600 * 1000;
        return new Date(Date.now() + days);
    }

    private names = [
        'apple', 'banana', 'coco nut', 'date plum', 'elderberry',
        'fig', 'grape', 'honeydew melon', 'iceberg lettuce'
    ];
}
