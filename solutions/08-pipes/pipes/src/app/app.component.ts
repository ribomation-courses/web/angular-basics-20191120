import {Component}                 from '@angular/core';
import {Product, ProductGenerator} from './product.domain';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  title               = 'using pipes';
  products: Product[] = [];
  count: number       = 8;

  constructor() {
    this.onUpdate();
  }

  onUpdate() {
    const gen     = new ProductGenerator();
    this.products = gen.generate(this.count);
  }
}
