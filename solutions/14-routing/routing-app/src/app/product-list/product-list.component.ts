import {Component}       from '@angular/core';
import {ProductsService} from '../products.service';
import {Router}          from '@angular/router';

@Component({
  selector:    'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls:   ['./product-list.component.css']
})
export class ProductListComponent {
  constructor(
    private productsSvc: ProductsService,
    private router: Router
  ) {
  }

  goto(id: number) {
    this.router.navigate(['/detail', id]);
  }
}
