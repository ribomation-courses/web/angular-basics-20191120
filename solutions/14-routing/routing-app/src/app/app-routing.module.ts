import {NgModule}                   from '@angular/core';
import {Route, RouterModule}        from '@angular/router';
import {HomeComponent}              from './home/home.component';
import {ProductListComponent}       from './product-list/product-list.component';
import {ProductDetailComponent}     from './product-detail/product-detail.component';
import {NotFoundComponent}          from './not-found/not-found.component';

const routes: Route[] = [
  {path: 'home', component: HomeComponent},
  {path: 'products', component: ProductListComponent},
  {path: 'detail/:id', component: ProductDetailComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
