import {Component, OnInit}      from '@angular/core';
import {ProductsService}        from '../products.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Product}                from '../product';
import {map, switchMap, tap}    from 'rxjs/operators';
import {of}                     from 'rxjs';

@Component({
  selector:    'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls:   ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: Product;

  constructor(
    private productsSvc: ProductsService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.product = this.productsSvc.findById(id);
  }

}
