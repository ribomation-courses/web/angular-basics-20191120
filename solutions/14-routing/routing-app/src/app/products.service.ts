import {Injectable} from '@angular/core';
import {Product}    from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  findAll(): Product[] {
    return this._products;
  }

  findById(id: number): Product {
    return this.findAll().find(p => p.id === id);
  }

  private readonly _products: Product[] = [
    {id: 1, name: 'Apple', price: 1},
    {id: 2, name: 'Banana', price: 1.5},
    {id: 3, name: 'Coco Nut', price: 2},
    {id: 4, name: 'Date Plum', price: 225},
  ];
}
