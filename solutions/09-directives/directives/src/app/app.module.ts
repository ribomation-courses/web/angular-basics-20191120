import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ShoppingCartViewComponent } from './shopping-cart-view/shopping-cart-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ShoppingCartViewComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
