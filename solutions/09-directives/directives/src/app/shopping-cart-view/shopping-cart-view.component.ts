import {Component, OnInit} from '@angular/core';

export class Item {
  constructor(
    public name: string,
    public price: number,
    public count: number
  ) {
  }

  get total(): number {
    return this.price * this.count;
  }
}

@Component({
  selector:    'app-shopping-cart-view',
  templateUrl: './shopping-cart-view.component.html',
  styleUrls:   ['./shopping-cart-view.component.css']
})
export class ShoppingCartViewComponent {
  items: Item[] = [];

  constructor() {
    this.toggle();
  }

  get total(): number {
    return this.items
      .map(p => p.total)
      .reduce((sum, price) => sum + price, 0);
  }

  toggle() {
    if (this.items.length > 0) {
      this.items = [];
    } else {
      this.items = [
        new Item('Apple', 2.5, 3),
        new Item('Banana', 1.25, 1),
        new Item('Coco Nut', 3.75, 5),
        new Item('Date Plum', 2.15, 4),
      ];
    }
  }

}
