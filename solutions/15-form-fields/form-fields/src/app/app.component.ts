import {Component, OnInit} from '@angular/core';
import {formatDate}        from '@angular/common';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  dateBase: Date   = new Date();
  dateResult: Date = new Date();
  offset: number   = 1;

  toDateText(): string {
    return formatDate(this.dateBase, 'yyyy-MM-dd', 'en');
  }

  ngOnInit(): void {
    this.dateResult = this.nextDate(this.dateBase, this.offset);
  }

  updateDate(val: string) {
    console.debug('val: %o', val);
    this.dateBase   = new Date(val);
    this.dateResult = this.nextDate(this.dateBase, this.offset);
  }

  updateOffset(val: number) {
    console.debug('val: %o', val);
    this.offset     = val;
    this.dateResult = this.nextDate(this.dateBase, this.offset);
  }

  private nextDate(date: Date, offset: number): Date {
    const DAY_MS = 24 * 3600 * 1000;
    return new Date(date.getTime() + offset * DAY_MS);
  }

}
