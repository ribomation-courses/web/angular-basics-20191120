import { Account } from './account';
import { Generator } from './generator'

const N = 100;
const accounts: Account[] = [];

for (let k = 0; k < N; ++k) {
    accounts.push(Generator.create());
}

for (let k = 0; k < accounts.length; ++k) {
    console.log(accounts[k].toString());
}
