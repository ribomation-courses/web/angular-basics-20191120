import {
    Component,
    OnInit,
    Input,
    Output,
    ViewChild,
    EventEmitter,
}                   from '@angular/core';
import {User}       from '../user';
import {FormGroup}  from '@angular/forms';
import {formatDate} from '@angular/common';

@Component({
    selector:    'template-oriented-form',
    templateUrl: './template-oriented-form.component.html',
    styleUrls:   ['./template-oriented-form.component.css']
})
export class TemplateOrientedFormComponent implements OnInit {
    @Input('user') init: User;
    @Output('save') saveEmitter     = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<void>();

    @ViewChild('userForm', {static: false})
    userForm: FormGroup;
    model: User;

    constructor() {
    }

    ngOnInit() {
        this.model = Object.assign({}, this.init);
    }

    onSave() {
        if (this.userForm.valid) {
            const formData = this.userForm.value;
            formData.anniversary = this.text2date(formData.anniversary);
            this.saveEmitter.emit(formData);
        }
    }

    onCancel() {
        this.cancelEmitter.emit();
    }

    date2text(d: Date): string {
        return formatDate(d, 'yyyy-MM-dd', 'en');
    }

    text2date(s: string): Date {
        return new Date(s);
    }

}
