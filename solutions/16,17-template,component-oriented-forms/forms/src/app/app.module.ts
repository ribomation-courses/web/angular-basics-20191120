import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }                   from './app.component';
import { ComponentOrientedFormComponent } from './component-oriented-form/component-oriented-form.component';
import { TemplateOrientedFormComponent }  from './template-oriented-form/template-oriented-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ComponentOrientedFormComponent,
    TemplateOrientedFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
