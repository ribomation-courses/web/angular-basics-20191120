export interface User {
  name: string;
  age?: number;
  female?: boolean;
  anniversary?: Date;
}

export function createUser(): User {
  return {
    name:   '',
    age:    0,
    female: false,
    anniversary:   new Date()
  };
}
