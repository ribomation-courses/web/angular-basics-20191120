import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User}                                           from '../user';
import {FormControl, FormGroup, Validators}             from '@angular/forms';
import {formatDate}                                     from '@angular/common';

@Component({
    selector:    'component-oriented-form',
    templateUrl: './component-oriented-form.component.html',
    styleUrls:   ['./component-oriented-form.component.css']
})
export class ComponentOrientedFormComponent implements OnInit {
    @Input('user') init: User;
    @Output('save') saveEmitter     = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<void>();

    userForm: FormGroup;
    nameCtrl: FormControl;
    ageCtrl: FormControl;
    femaleCtrl: FormControl;
    anniversaryCtrl: FormControl;

    constructor() {
    }

    ngOnInit() {
        this.nameCtrl        = new FormControl(this.init.name, [
            Validators.required, Validators.minLength(4), Validators.maxLength(10)
        ]);
        this.ageCtrl         = new FormControl(this.init.age, [
            Validators.required, Validators.min(20), Validators.max(80)
        ]);
        this.femaleCtrl      = new FormControl(this.init.female);
        this.anniversaryCtrl = new FormControl(this.date2text(this.init.anniversary), [
            Validators.required
        ]);

        this.userForm = new FormGroup({
            name:        this.nameCtrl,
            age:         this.ageCtrl,
            female:      this.femaleCtrl,
            anniversary: this.anniversaryCtrl
        });
    }

    onSave() {
        if (this.userForm.valid) {
            const formData = this.userForm.value;
            formData.anniversary = this.text2date(formData.anniversary);
            this.saveEmitter.emit(formData);
        }
    }

    onCancel() {
        this.cancelEmitter.emit();
    }

    date2text(d: Date): string {
        return formatDate(d, 'yyyy-MM-dd', 'en');
    }

    text2date(s: string): Date {
        return new Date(s);
    }

}
