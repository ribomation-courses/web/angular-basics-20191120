import {Component, OnInit} from '@angular/core';
import {createUser, User}  from './user';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  user: User;

  ngOnInit(): void {
    this.user = createUser();
    this.user.name = 'Anna Conda';
    this.user.age = 42;
  }

  onSubmit(usr: User) {
    console.info('[onSumbit] usr: %o', usr);
    this.user = usr;
  }

  onCancel() {
    console.info('cancelled');
  }

}
