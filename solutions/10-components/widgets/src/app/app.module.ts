import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }           from './app.component';
import { NumberStepperComponent } from './number-stepper/number-stepper.component';
import { EditableTextComponent }  from './editable-text/editable-text.component';
import {FormsModule}              from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NumberStepperComponent,
    EditableTextComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
