import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'number-stepper',
  templateUrl: './number-stepper.component.html',
  styleUrls: ['./number-stepper.component.css']
})
export class NumberStepperComponent {
  @Output('valueChange') out = new EventEmitter<number>();
  @Input('value') theValue: number | string = 5;
  @Input('min')   minValue: number | string = 1;
  @Input('max')   maxValue: number | string = 25;

  dec() {
    this.resize(-1);
  }

  inc() {
    this.resize(+1);
  }

  resize(delta: number) {
    const VAL:number = +(this.theValue);
    const MIN:number = +(this.minValue);
    const MAX:number = +(this.maxValue);

    const nextValue = VAL + delta;
    this.theValue   = Math.min(Math.max(+MIN, nextValue), MAX);

    this.out.emit(this.theValue);
  }
}
