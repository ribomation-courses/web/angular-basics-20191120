import {Component} from '@angular/core';
import {User}      from './domain/user';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  title = 'widgets demo';

  fontSize = 1;
  txt      = 'foobar strikes again';

  user = new User('Ana Gram', 42, undefined);

  onUpdate(key: string, value: any) {
    console.info(`** update: ${key}='${value}'`);
    setTimeout(() => {
      alert(`Updated: ${key}=${value}`);
    }, 500);
  }

  onCancel() {
    console.info('cancelled');
  }

}
