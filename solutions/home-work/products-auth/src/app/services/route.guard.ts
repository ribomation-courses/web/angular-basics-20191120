import {Injectable}            from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree, Router
}                              from '@angular/router';
import {Observable}            from 'rxjs';
import {AuthenticationService} from "./authentication.service";

@Injectable({
    providedIn: 'root'
})
export class RouteGuard implements CanActivate {
    constructor(
        private authSvc: AuthenticationService,
        private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        if (this.authSvc.signedIn) {
            return true;
        } else {
            this.router.navigate(['/home']);
            return false;
        }
    }

}
