import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor() { }

  get name(): string {
    return 'Whatever Services, Ltd.';
  }

  get logo(): string {
    return 'assets/img/logo.png';
  }

  get year(): number {
    return new Date().getFullYear();
  }

}
