import {Injectable}                    from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {of}                         from "rxjs";
import {catchError, map, take, tap} from 'rxjs/operators';
import {User}                       from "../domain/user";
import {Router}                        from "@angular/router";

export interface LoginResponse {
    token: string;
    user: User;
    date: Date;
}

export interface LoginFailure {
    status: number;
    message: string;
}

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private readonly loginUrl: string = 'http://localhost:3000/login';
    private readonly KEY: string      = 'login-response';
    private currentUser: LoginResponse;

    constructor(private http: HttpClient, private router: Router) {
        const usr = this.load();
        if (usr) {
            this.currentUser = usr;
        }
    }

    replace(updatedUser: User) {
        this.currentUser.user = updatedUser;
        this.save();
    }

    signIn(username: string, password: string): Promise<boolean> {
        return this.http
            .post<LoginResponse | LoginFailure>(this.loginUrl, {username, password})
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    const payload: LoginFailure = {
                        status:  err.status,
                        message: err.message
                    };
                    return of(payload);
                }),
                take(1),
                tap(rply => console.debug('[auth] reply: %o', rply)),
                map((rply: any) => {
                    if (rply.token) {
                        this.currentUser = rply;
                        this.save();
                        return true;
                    } else {
                        this.currentUser = undefined;
                        this.clear();
                        return false;
                    }
                })
            )
            .toPromise();
    }

    signOut() {
        this.currentUser = undefined;
        this.clear();
        this.router.navigate(['/home']);
    }

    get user(): User {
        return this.currentUser.user;
    }

    get signedIn(): boolean {
        return !!(this.currentUser);
    }

    get token(): string {
        return this.currentUser.token;
    }

    get header(): string {
        return 'X-Auth-Token';
    }

    private save() {
        if (this.signedIn) {
            sessionStorage.setItem(this.KEY, JSON.stringify(this.currentUser));
        }
    }

    private load() {
        const usr = sessionStorage.getItem(this.KEY);
        if (usr) {
            return JSON.parse(usr);
        } else {
            return undefined;
        }
    }

    private clear() {
        sessionStorage.removeItem(this.KEY);
    }

}
