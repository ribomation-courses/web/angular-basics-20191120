import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ToastService {
    private visible_: boolean = false;
    private message_: string;
    private type_: string;

    get visible(): boolean {
        return this.visible_;
    }

    get type(): string {
        return this.type_;
    }

    get message(): string {
        return this.message_;
    }

    showError(message: string) {
        this.show('error', message, 5000);
    }

    showSuccess(message: string) {
        this.show('success', message, 2000);
    }

    private show(type: string, message: string, interval: number) {
        this.type_    = type;
        this.message_ = message;
        if (!this.visible_) setTimeout(() => this.visible_ = false, interval);
        this.visible_ = true;
    }

}
