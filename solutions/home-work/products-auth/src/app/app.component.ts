import {Component, OnInit}                     from '@angular/core';
import {ToastService}                          from "./services/toast.service";
import {Title}                                 from "@angular/platform-browser";
import {ActivatedRoute, NavigationEnd, Router}   from "@angular/router";
import {delay, filter, map, mergeMap, startWith} from "rxjs/operators";

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(
        public toastSvc: ToastService,
        private titleSvc: Title,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        const firstChildOf = (r: ActivatedRoute): ActivatedRoute => {
            while (r.firstChild) r = r.firstChild;
            return r;
        };

        this.router.events
            .pipe(
                startWith(new NavigationEnd(0, '', '')),
                delay(0),
                filter(ev => ev instanceof NavigationEnd),
                map(_ => firstChildOf(this.route)),
                filter(r => r.outlet === 'primary'),
                mergeMap(r => r.data),
                filter(data => !!data)
            )
            .subscribe(routeData => {
                this.titleSvc.setTitle(`${routeData.title || ''} :: Whatever `)
            });
    }

}
