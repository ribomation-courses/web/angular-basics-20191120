//{
// "id":1,
// "name":"Smoked Paprika",
// "price":8.19,
// "count":13,
// "image":"https://picsum.photos/id/1/200/200"
// }

export interface Product {
  id: number;
  name: string;
  price: number;
  count: number;
  image?: string;
}
