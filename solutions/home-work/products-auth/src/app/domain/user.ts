
export interface User {
  id: number;
  username: string;
  password?: string;
  displayName?: string;
  age: number;
  female: boolean;
  birthDay: Date | string;
  profileImage: string;
}

