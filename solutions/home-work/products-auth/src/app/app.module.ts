import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule }      from './app-routing.module';
import { AppComponent }          from './app.component';
import { HomeComponent }         from './pages/home/home.component';
import { ProductsComponent }     from './pages/products/products.component';
import { ProfileComponent }      from './pages/profile/profile.component';
import { NotFoundComponent }     from './pages/not-found/not-found.component';
import { HeaderComponent }       from './layout/header/header.component';
import { FooterComponent }       from './layout/footer/footer.component';
import { SidebarComponent }      from './layout/sidebar/sidebar.component';
import { EditableTextComponent } from './widgets/editable-text/editable-text.component';
import {FormsModule}             from "@angular/forms";
import { EditableNumberComponent } from './widgets/editable-number/editable-number.component';
import { EditableDateComponent } from './widgets/editable-date/editable-date.component';
import { EditableBooleanComponent } from './widgets/editable-boolean/editable-boolean.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ProfileComponent,
    NotFoundComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    EditableTextComponent,
    EditableNumberComponent,
    EditableDateComponent,
    EditableBooleanComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
