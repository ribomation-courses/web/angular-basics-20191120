import {NgModule}            from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {HomeComponent}       from "./pages/home/home.component";
import {ProductsComponent}   from "./pages/products/products.component";
import {ProfileComponent}    from "./pages/profile/profile.component";
import {NotFoundComponent}   from "./pages/not-found/not-found.component";
import {RouteGuard}          from "./services/route.guard";

const routes: Route[] = [
    {path: 'home', component: HomeComponent, data: {title: 'Welcome'}},
    {path:           'products',
        component:   ProductsComponent,
        canActivate: [RouteGuard],
        data:        {title: 'Product List'}
    },
    {path:           'profile',
        component:   ProfileComponent,
        canActivate: [RouteGuard],
        data:        {title: 'Your Profile'}
    },
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundComponent, data: {title: 'Page Not Found'}}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
