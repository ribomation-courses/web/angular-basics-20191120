import {Component, OnInit} from '@angular/core';
import {EditableBase}      from "../editable.base";

@Component({
  selector:    'editable-text',
  templateUrl: '../editable.base.html',
  styleUrls:   ['../editable.base.scss']
})
export class EditableTextComponent extends EditableBase implements OnInit {

  fieldType(): string {
    return 'text';
  }

  ngOnInit(): void {
    super.minValue = '';
    super.maxValue = '';
  }

}
