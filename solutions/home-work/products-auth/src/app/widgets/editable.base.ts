import {EventEmitter, Input, Output} from "@angular/core";

export type PropertyType = string | number | boolean | Date;

export abstract class EditableBase {
  @Input('value')       value: PropertyType;
  @Input('placeholder') placeholder: string = 'Click to edit';
  @Input('min') minValue: PropertyType    = 1;
  @Input('max') maxValue: PropertyType    = 100;

  @Output('update')     updateEmitter = new EventEmitter<PropertyType>();
  @Output('cancel')     cancelEmitter = new EventEmitter<void>();

  abstract fieldType(): string;

  formVisible: boolean = false;
  valueOrig: PropertyType;
  valueForm: PropertyType;

  get hasValue(): boolean {
    return this.value !== undefined
      && this.value !== null
      && this.value.toString().trim().length > 0;
  }

  showForm() {
    this.valueOrig = this.value;
    this.valueForm = this.value;
    this.formVisible = true;
  }

  onKeyup(ev: any) {
    if (ev.key === 'Enter') {
      this.save();
    } else if (ev.key === 'Escape') {
      this.cancel();
    }
  }

  onChanged(val:any) {
    this.formVisible = false;
    this.value = val.target.checked || false;
    this.valueOrig = undefined;
    this.valueForm = undefined;
    this.updateEmitter.emit(this.value);
  }

  save() {
    this.formVisible = false;

    this.value = this.valueForm;
    this.valueOrig = undefined;
    this.valueForm = undefined;

    this.updateEmitter.emit(this.value);
  }

  cancel() {
    this.formVisible = false;

    this.value = this.valueOrig;
    this.valueOrig = undefined;
    this.valueForm = undefined;

    this.cancelEmitter.emit();
  }
}

