import {Component}    from '@angular/core';
import {EditableBase} from "../editable.base";

@Component({
  selector:    'editable-number',
  templateUrl: '../editable.base.html',
  styleUrls:   ['../editable.base.scss']
})
export class EditableNumberComponent extends EditableBase {

  fieldType(): string {
    return 'number';
  }

}
