import { Component, OnInit } from '@angular/core';
import {EditableBase}        from "../editable.base";

@Component({
  selector: 'editable-date',
  templateUrl: '../editable.base.html',
  styleUrls:   ['../editable.base.scss']
})
export class EditableDateComponent extends EditableBase implements OnInit {

  fieldType(): string {
    return 'date';
  }

  ngOnInit() {

  }

}
