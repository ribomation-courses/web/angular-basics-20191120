import { Component, OnInit } from '@angular/core';
import {EditableBase}        from "../editable.base";

@Component({
  selector: 'editable-boolean',
  templateUrl: '../editable.base.html',
  styleUrls:   ['../editable.base.scss']
})
export class EditableBooleanComponent extends EditableBase {

  fieldType(): string {
    return 'checkbox';
  }

}
