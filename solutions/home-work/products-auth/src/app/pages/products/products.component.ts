import {Component, OnInit} from '@angular/core';
import {ProductsService}   from "../../services/products.service";
import {Product}           from "../../domain/product";

@Component({
  selector:    'app-products',
  templateUrl: './products.component.html',
  styleUrls:   ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];

  constructor(private prodSvc: ProductsService) {
  }

  ngOnInit() {
    this.prodSvc.list()
      .then(objs => this.products = objs);
  }

}
