import {Component}      from '@angular/core';
import {CompanyService} from "../../services/company.service";

@Component({
    selector:    'app-footer',
    templateUrl: './footer.component.html',
    styleUrls:   ['./footer.component.scss']
})
export class FooterComponent {

    constructor(public companySvc:CompanyService) {
    }

}
