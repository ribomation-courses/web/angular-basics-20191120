import {Component}     from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector:    'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls:   ['./sidebar.component.scss']
})
export class SidebarComponent {

  constructor(private auth: AuthenticationService) {
  }

  get signedIn(): boolean {
    return this.auth.signedIn;
  }

  get displayName(): string {
    return this.auth.user.displayName;
  }

  get profileImage(): string {
    return this.auth.user.profileImage;
  }

}
