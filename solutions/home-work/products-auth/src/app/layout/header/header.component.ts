import {Component}             from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {ToastService}          from "../../services/toast.service";
import {CompanyService}        from "../../services/company.service";

@Component({
    selector:    'app-header',
    templateUrl: './header.component.html',
    styleUrls:   ['./header.component.scss']
})
export class HeaderComponent {
    constructor(
        private auth: AuthenticationService,
        private toast: ToastService,
        public companySvc:CompanyService) {
    }

    async signIn(username: string, password: string) {
        const result = await this.auth.signIn(username, password);
        if (result) {
            this.toast.showSuccess(`Welcome ${username}`);
        } else {
            this.toast.showError(`Failed to login`);
        }
    }

    signOut() {
        this.auth.signOut();
        this.toast.showSuccess(`Bye. Hope to see you soon again.`);
    }

    get signedIn(): boolean {
        return this.auth.signedIn;
    }

    get displayName(): string {
        return this.auth.user.displayName;
    }

}
