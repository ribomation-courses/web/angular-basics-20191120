// Information for client-side implementation
// login URL     : /login
// Login Response: { token: JWT, user: username, date: new Date() }
// Request Header: X-Auth-Token

const FS     = require('fs');
const JWT    = require('njwt');
const RANDOM = require('secure-random');

const TOKEN_REQ_HDR = 'X-Auth-Token';
let TOKEN_KEY;


function isAuthenticationNotRequired(req) {
    const METHODS = ['GET', 'OPTIONS'];
    const URIS    = ['/products'];
    return METHODS.includes(req.method) && URIS.includes(req.path);
}

function isLoginRequest(req) {
    return req.method === 'POST' && req.originalUrl === '/login';
}

function isTokenVerified(req) {
    const token = req.get(TOKEN_REQ_HDR);
    if (token) {
        try {
            JWT.verify(token, TOKEN_KEY);
            return true;
        } catch (x) {
        }
    }
    return false;
}

function findUser(username, password) {
    const db = JSON.parse(FS.readFileSync(__dirname + '/db.json', 'utf8'));
    return db.users.find(u => u.username === username && u.password === password);
}

function strip(target, forbiddenPropertyKeys) {
    const propertyPairs = Object.entries(target)
        .filter(keyVal => !forbiddenPropertyKeys.includes(keyVal[0]));
    return Object.fromEntries(propertyPairs);
}

function handleLogin(req, res, next) {
    let username = req.body.username;
    let password = req.body.password;
    const user   = findUser(username, password);
    if (user) {
        const claims = {
            iss: 'whatever-services',
            sub: strip(user, ['password'])
        };
        res.status(201).json({
            token: JWT.create(claims, TOKEN_KEY).compact(),
            user: user,
            date: new Date()
        });
    } else {
        res.sendStatus(401);
    }
}

function handleAllRequests(req, res, next) {
    if (isAuthenticationNotRequired(req)) {
        next();
    } else if (isLoginRequest(req)) {
        handleLogin(req, res, next);
    } else if (isTokenVerified(req)) {
        next();
    } else {
        res.sendStatus(401);
    }
}

function initServer() {
    const keyFile = __dirname + '/token-key.txt';
    if (FS.existsSync(keyFile)) {
        TOKEN_KEY = FS.readFileSync(keyFile).toString('base64');
        console.info('loaded token key from file: %s', keyFile);
    } else {
        TOKEN_KEY = RANDOM(256, {type: 'Buffer'});

        FS.writeFileSync(keyFile, TOKEN_KEY.toString('base64'));
        console.info('saved token key to file: %s', keyFile);
    }
    console.debug('token key: %s', TOKEN_KEY.toString('base64'));
}

module.exports = handleAllRequests;
initServer();
