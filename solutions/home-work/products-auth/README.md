# Solution to the Home-Work
This version has been augmented with 

* Authentication using a middleware plugg-in to json-server
* Router guard, to prevent navigating to e.g. `/profile`, when not logged in
* Redirection to `/home`, after logout
* Toast service, that can show error and success messages
* Company service, to provide the name, logo etc
* Environment, to show different images depending on dev/prod mode
* Auth middleware, rewritten to load credentials from `db.json` and save the key in a file

## Running in DEV mode
Start the server

    npm run start-server

Then, start the development client

    npm run start-client

## Running in PROD mode
Build first the client

    npm run build

Then start the server

    npm run start-server

Finally, start the production client

    npm run start-client-prod


