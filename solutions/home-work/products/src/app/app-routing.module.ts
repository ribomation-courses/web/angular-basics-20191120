import {NgModule}            from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {HomeComponent}       from "./pages/home/home.component";
import {ProductsComponent}   from "./pages/products/products.component";
import {ProfileComponent}    from "./pages/profile/profile.component";
import {NotFoundComponent}   from "./pages/not-found/not-found.component";

const routes: Route[] = [
  {path: 'home', component: HomeComponent},
  {path: 'products', component: ProductsComponent},
  {path: 'profile', component: ProfileComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
