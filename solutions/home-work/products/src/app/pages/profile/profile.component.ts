import {Component}      from '@angular/core';
import {ProfileService} from "../../services/profile.service";

@Component({
  selector:    'app-profile',
  templateUrl: './profile.component.html',
  styleUrls:   ['./profile.component.scss']
})
export class ProfileComponent {
  constructor(public user: ProfileService) {
  }

  updateProperty(key: string, val: any) {
    console.info('update(%s, %o)', key, val);
    this.user.update(key, val)
      .then(usr => console.info('updated: %o', usr))
  }

}
