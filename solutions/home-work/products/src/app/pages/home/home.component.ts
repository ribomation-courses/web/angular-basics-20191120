import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  image = 'https://picsum.photos/id/1043/800/500';

  constructor() { }

  ngOnInit() {
  }

}
