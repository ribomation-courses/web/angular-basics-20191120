import {Injectable}            from '@angular/core';
import {AuthenticationService} from "./authentication.service";
import {User}                  from "../domain/user";
import {HttpClient}            from "@angular/common/http";
import {take, tap}             from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  readonly baseUrl = 'http://localhost:3000/users/';

  constructor(
    private authSvc: AuthenticationService,
    private httpSvc: HttpClient) {
  }

  get profile(): User {
    return this.authSvc.currentUser;
  }

  update(key: string, val: string | number | boolean): Promise<User> {
    const id           = this.profile.id;
    const payload: any = {};
    payload[key]       = val;
    return this.httpSvc.patch<User>(this.baseUrl + id, payload)
      .pipe(
        take(1),
        tap(reply => console.debug('** reply: %o', reply)),
        tap((usr: User) => this.authSvc.replace(usr))
      )
      .toPromise();
  }

}
