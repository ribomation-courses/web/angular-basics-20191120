import {Injectable}             from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User}                   from "../domain/user";
import {map, take, tap}         from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  readonly baseUrl: string = 'http://localhost:3000/users';
  currentUser: User;

  constructor(private http: HttpClient) {
    const usr = this.load();
    if (usr) {
      this.currentUser = usr;
    }
  }

  replace(updatedUser: User) {
    this.currentUser = updatedUser;
    this.save();
  }

  signIn(username: string, password: string): Promise<User> {
    const params = new HttpParams()
      .append('username', username)
      .append('password', password)
    ;
    return this.http
      .get<User>(this.baseUrl, {params})
      .pipe(
        take(1),
        map((reply: any) => reply && reply.length > 0 ? reply[0] : null),
        tap(usr => this.currentUser = usr),
        tap(usr => {
          if (usr) this.save();
        })
      )
      .toPromise()
      ;
  }

  signOut() {
    this.currentUser = undefined;
    this.clear()
  }

  get user(): User {
    return {
      id:           this.currentUser.id,
      username:     this.currentUser.username,
      displayName:  this.currentUser.displayName,
      profileImage: this.currentUser.profileImage,
      age:          this.currentUser.age,
      birthDay:     this.currentUser.birthDay,
      female:       this.currentUser.female,
      password:     ''
    };
  }

  get signedIn(): boolean {
    return !!(this.currentUser);
  }

  private readonly KEY = 'user';

  private save() {
    if (this.signedIn) {
      sessionStorage.setItem(this.KEY, JSON.stringify(this.user));
    }
  }

  private load() {
    const usr = sessionStorage.getItem(this.KEY);
    if (usr) {
      return JSON.parse(usr);
    } else {
      return undefined;
    }
  }

  private clear() {
    sessionStorage.removeItem(this.KEY);
  }

}
