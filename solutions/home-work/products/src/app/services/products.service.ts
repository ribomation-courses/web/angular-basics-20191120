import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product}    from "../domain/product";
import {take}       from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  readonly baseUrl = 'http://localhost:3000/products';

  constructor(private http: HttpClient) {
  }

  list(): Promise<Product[]> {
    return this.http
      .get<Product[]>(this.baseUrl)
      .pipe(
        take(1)
      )
      .toPromise()
    ;
  }

}
