import {Component}     from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector:    'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls:   ['./sidebar.component.scss']
})
export class SidebarComponent {

  constructor(public auth: AuthenticationService) {
  }

  get signedIn(): boolean {
    return this.auth.signedIn;
  }
}
