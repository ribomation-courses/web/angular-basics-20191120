import {Component, OnInit}     from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector:    'app-header',
  templateUrl: './header.component.html',
  styleUrls:   ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public auth: AuthenticationService) {
  }

  ngOnInit() {
  }

  async signIn(username: string, password: string) {
    const result = await this.auth.signIn(username, password);
    if (result) {
      console.info('signed in: %o', result);
    } else {
      console.warn('failed to login: %s/%s', username, password);
    }
  }

  signOut() {
    this.auth.signOut();
  }

  get signedIn(): boolean {
    return this.auth.signedIn;
  }

}
