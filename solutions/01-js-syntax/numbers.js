
function sum(n) {
    if (n <= 0) return 0;
    return n + sum(n - 1);
}
function prod(n) {
    if (n <= 1) return 1;
    return n * prod(n - 1);
}
function fib(n) {
    if (n <= 0) return 0;
    if (n === 1) return 1;
    return fib(n - 2) + fib(n - 1);
}
function table(n) {
    for (let k = 1; k <= n; ++k) {
        console.log('%d\t%d\t%d\t%d', k, sum(k), fib(k), prod(k));
    }
}


// var / let / const
const N = 42;
// console.log('sum(%d) = %d', N, sum(N));
// console.log('prod(%d) = %d', N, prod(N));
// console.log('fib(%d) = %d', N, fib(N));
table(N);