import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';
import {FormsModule}   from '@angular/forms';

import {AppComponent} from './app.component';
import {factorToken}  from './services/factor.token';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports:      [
    BrowserModule, FormsModule
  ],
  providers:    [
    {provide: factorToken, useValue: 42}
  ],
  bootstrap:    [AppComponent]
})
export class AppModule {}
