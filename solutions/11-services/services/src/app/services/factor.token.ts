import {InjectionToken} from '@angular/core';

export const factorToken = new InjectionToken<number>('Factor to multiply');
