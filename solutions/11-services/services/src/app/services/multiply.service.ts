import {Inject, Injectable} from '@angular/core';
import {factorToken}        from './factor.token';

@Injectable({
  providedIn: 'root'
})
export class MultiplyService {
  private _factor: number;

  constructor(@Inject(factorToken) init: number) {
    this._factor = init || 3;
  }

  compute(arg: number): number {
    return arg * this._factor;
  }

  get factor(): number {
    return this._factor;
  }

  set factor(n: number) {
    this._factor = +n;
  }
}
