const payload = new Date().toString();
console.log('"%s".length = %d',
    payload, payload.length);

console.log('upper: %s',
    payload.toUpperCase());

console.log('replaced: %s',
    payload.replace(/\d/g, '#').replace(/\s/g, ''));

console.log('repeated: %s',
    payload.repeat(5));

const N = payload.length;
console.log('middle third: %s',
    payload.substr(N/3, N/3));
