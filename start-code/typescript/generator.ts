...


export class Generator {
    static create(): Account {
        const accno     = this.nextAccno();
        const balance   = this.nextInt(-10, +100);
        const hasCredit = (Math.random() <= 0.6);
        const date      = new Date(Date.now() - this.nextInt(5, 90) * 24 * 3600 * 1000);
        const customer  = this.nextElem(this.names);

        return new Account(accno, balance, hasCredit, date, customer);
    }

    static nextAccno() {
        return ...;
    }

    static nextInt(lb: number, ub: number): number {
        if (ub <= lb) throw new Error('invalid number range');
        const interval = ub - lb + 1;
        const value    = Math.random() * interval + lb;
        return Math.floor(value);
    }

    static nextInts(n: number, lb: number, ub: number): string {
        let result = '';
        while (n-- > 0) result += this.nextInt(lb, ub).toString(10);
        return result;
    }

    static nextElem(arr: string[]): string {
        return arr[this.nextInt(0, arr.length - 1)];
    }

    static banks: string[] = [...];
    static names: string[] = [...];
}

