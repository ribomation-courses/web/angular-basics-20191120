export class Account {
    public static rate: number = 0.25;

    constructor(
        private _accno: string,
        ...
    ) {  }

    get accno(): string {
        return this._accno;
    }

    get balance(): number {
        ...
    }

    get credit(): boolean {
        ...
    }

    get created(): Date {
        ...
    }

    get owner(): string {
        ...
    }

    update(amount: number): void {
        const newBalance = this.balance + amount;
        if (newBalance >= 0 || this.credit) this._balance = newBalance;
    }

    toString(): string {
        return `Account[${this.accno}, SEK ${this.balance}, ${this.created.toDateString()}, ${this.owner}]`;
    }

}
