import { Component, Input, Output, EventEmitter } from '@angular/core';

type PropertyType = string | number | boolean;

@Component({
  selector: 'editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls: ['./editable-text.component.scss']
})
export class EditableTextComponent {
  @Input('value')       value: PropertyType;
  @Input('placeholder') placeholder: string = 'Click to edit';
  @Output('update')     updateEmitter = new EventEmitter<PropertyType>();
  @Output('cancel')     cancelEmitter = new EventEmitter<void>();
  
  formVisible: boolean = false;
  valueOrig: PropertyType;
  valueForm: PropertyType;

  get hasValue(): boolean {
    //...
  }

  onKeyup(ev: any) {
    //...
  }

  showForm() {
    //...
  }

  save() {
    //...
  }

  cancel() {
    //...
  }

}
