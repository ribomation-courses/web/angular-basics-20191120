import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//...

@NgModule({
  declarations: [
    AppComponent,
    //...
  ],
  imports: [
    BrowserModule,
    FormsModule  // needed for usage of [(ngModel)]
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
